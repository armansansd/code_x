# note on styling markup and other fonctionality


### html tag

* the page    : ```.pagedjs_page```
* the content : ```.pagedjs_page_content```
* the margin  : ```.pagedjs_margin-top-left-corner .pagedjs_margin-top .pagedjs_margin-top-right-corner ...```
* the bleed   : ```.pagedjs_bleed .pagedjs_bleed-right / top left bottom```
* has Content : ```.hasContent```


### css

1. the page margin

```css
@page :left{}
@top-left{}
@top-left-corner{}
```


2. header

```css
position: running(booktitle);
```

3. page breaks

```css
break-before:right-left-both;
break-inside: avoid;
```

4. Page name and identification

```css
@page :blank{}
```

5. fine tuning

> only on macosx
```css
widows: 3;
orphans: 3;

hyphens: none;
hyphens: manual;
hyphens: auto;

```
